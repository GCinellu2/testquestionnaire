require 'test_helper'

class UserQuestionnairesControllerTest < ActionController::TestCase
  setup do
    @user_questionnaire = user_questionnaires(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:user_questionnaires)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create user_questionnaire" do
    assert_difference('UserQuestionnaire.count') do
      post :create, user_questionnaire: { questionnaire_id: @user_questionnaire.questionnaire_id, user_id: @user_questionnaire.user_id }
    end

    assert_redirected_to user_questionnaire_path(assigns(:user_questionnaire))
  end

  test "should show user_questionnaire" do
    get :show, id: @user_questionnaire
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @user_questionnaire
    assert_response :success
  end

  test "should update user_questionnaire" do
    patch :update, id: @user_questionnaire, user_questionnaire: { questionnaire_id: @user_questionnaire.questionnaire_id, user_id: @user_questionnaire.user_id }
    assert_redirected_to user_questionnaire_path(assigns(:user_questionnaire))
  end

  test "should destroy user_questionnaire" do
    assert_difference('UserQuestionnaire.count', -1) do
      delete :destroy, id: @user_questionnaire
    end

    assert_redirected_to user_questionnaires_path
  end
end
