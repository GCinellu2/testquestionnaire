class CreateQuestions < ActiveRecord::Migration
  def change
    create_table :questions do |t|
      t.string :name
      t.belongs_to :questionnaire, index: true
      t.string :question_type

      t.timestamps
    end
  end
end
