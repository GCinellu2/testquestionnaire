class CreateUserQuestionnaires < ActiveRecord::Migration
  def change
    create_table :user_questionnaires do |t|
      t.belongs_to :user, index: true
      t.belongs_to :questionnaire, index: true

      t.timestamps
    end
  end
end
