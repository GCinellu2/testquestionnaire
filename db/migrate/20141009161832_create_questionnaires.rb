class CreateQuestionnaires < ActiveRecord::Migration
  def change
    create_table :questionnaires do |t|
      t.string :name
      t.text :desc
      t.integer :max_questions, default: 15

      t.timestamps
    end
  end
end
