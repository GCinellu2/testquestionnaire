class AddFieldToUser < ActiveRecord::Migration
  def change
    add_column :users, :is_admin, :boolean
    change_column_default :users, :is_admin, false
  end
end
