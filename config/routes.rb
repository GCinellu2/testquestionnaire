Rails.application.routes.draw do
  devise_for :users
  resources :users

  resources :user_questionnaires
  resources :user_answers

  resources :questionnaires do
    resources :questions do
      resources :answers
    end
  end

  root 'questionnaires#index'
end
