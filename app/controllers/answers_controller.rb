class AnswersController < ApplicationController
  before_filter :set_questionnaire
  before_filter :set_question
  before_action :set_answer, only: [:edit, :update, :destroy]

  def index
    @answers = @question.answers
  end

  def new
    @answer = @question.answers.new
  end

  def edit
  end

  def create
    @answer = @question.answers.new(answer_params)

    respond_to do |format|
      if @answer.save
        format.html { redirect_to questionnaire_question_path(@questionnaire, @question), notice: 'Answer was successfully created.' }
      else
        format.html { render :new }
      end
    end
  end

  def update
    respond_to do |format|
      if @answer.update(answer_params)
        format.html { redirect_to questionnaire_question_answer_path(@questionnaire, @question, @answer), notice: 'Answer was successfully updated.' }
      else
        format.html { render :edit }
      end
    end
  end

  def destroy
    @answer.destroy
    respond_to do |format|
      format.html { redirect_to answers_url, notice: 'Answer was successfully destroyed.' }
    end
  end

  private
  def set_answer
    @answer = Answer.find(params[:id])
  end

  def set_questionnaire
    @questionnaire = Questionnaire.find(params[:questionnaire_id])
  end

  def set_question
    @question = @questionnaire.questions.find(params[:question_id])
  end

  def answer_params
    params.require(:answer).permit(:question_id, :value)
  end
end
