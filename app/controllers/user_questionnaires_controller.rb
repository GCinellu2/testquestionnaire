class UserQuestionnairesController < ApplicationController
  before_action :set_user_questionnaire, only: [:show, :edit, :update, :destroy]
  skip_before_action :check_admin!

  def index
    @questionnaires = Questionnaire.all
    @user_questionnaires = UserQuestionnaire.all
  end

  def show
    @questionnaire = Questionnaire.find(params[:id])
    @questions = @questionnaire.questions
  end

  def new
    @user_questionnaire = UserQuestionnaire.new
  end

  def edit
    @questionnaire = Questionnaire.find(params[:id])
    @questions = @questionnaire.questions
    @user_answers = UserAnswer.new
  end

  def create
    @questionnaire = Questionnaire.find(params[:questionnaire_id])
    @user_questionnaire = UserQuestionnaire.find_or_create_by(user: current_user, questionnaire: @questionnaire)

    redirect_to edit_user_questionnaire_path(@user_questionnaire)
  end

  def update
    uq = UserQuestionnaire.find params[:id]

    params[:question].each do |k,v|
      q = Question.find k

      ua = UserAnswer.find_or_initialize_by(user: current_user, question: q)

      case q.question_type
      when 'open'
        ua.value = v
      when 'multiple'
        ua.value = v.values.join(',')
      else
        a = q.answers.find_by_value v
        ua.answer = a
      end

      ua.save
      raise ua.errors.inspect unless ua.save
    end

    redirect_to uq
  end

  def destroy
    @user_questionnaire.destroy
    respond_to do |format|
      format.html { redirect_to user_questionnaires_url, notice: 'User questionnaire was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user_questionnaire
      @user_questionnaire = UserQuestionnaire.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_questionnaire_params
      params.require(:user_questionnaire).permit(:user_id, :questionnaire_id)
    end
end
