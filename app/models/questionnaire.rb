class Questionnaire < ActiveRecord::Base
  has_many :questions
  def full?
    self.questions.count >= self.max_questions
  end
end
